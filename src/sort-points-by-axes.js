/*
  возвращает новый массив отсортированных точек
  axes - массив осей в нужном порядке (x, y, z)
  orders - массив для настройки сортировки по каждой оси (false - по возрастанию, true - по убыванию)
*/

export const sortPointsByAxes = (points, axes, orders = [false, false, false]) => {
  const sortedPoints = [...points]

  return sortedPoints.sort((a, b) => {
    for (let i = 0; i < axes.length; i++) {
      const axis = axes[i]
      const order = orders[i]

      if (a[axis] === b[axis]) {
        continue
      }

      return order
        ? b[axis] - a[axis]
        : a[axis] - b[axis]
    }
  })
}
