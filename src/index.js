import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { Text } from 'troika-three-text'

import { sortPointsByAxes } from './sort-points-by-axes'

const POINT_POSITION = 50
const width = 1000
const height = 500

const renderer = new THREE.WebGLRenderer()

renderer.setClearColor('#eee')
renderer.setSize(width, height)
document.body.appendChild(renderer.domElement)

const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(50, width / height)

camera.position.z = POINT_POSITION * 2

const controls = new OrbitControls(camera, renderer.domElement)

controls.update()

const axesHelper = new THREE.AxesHelper(POINT_POSITION)

scene.add(axesHelper)

let points = []

for (let i = 0; i < 10; i++) {
  const x = THREE.MathUtils.randFloat(0, POINT_POSITION)
  const y = THREE.MathUtils.randFloat(0, POINT_POSITION)
  const z = THREE.MathUtils.randFloat(0, POINT_POSITION)

  points.push({ x, y, z })
}

// points.push(
//   { x: POINT_POSITION / 3 * 2, y: 0, z: 0 },
//   { x: POINT_POSITION / 3, y: 0, z: 0 },
//   { x: POINT_POSITION / 3, y: POINT_POSITION / 3, z: 0 },
//   { x: POINT_POSITION / 3 * 2, y: POINT_POSITION / 3, z: 0 },

//   { x: POINT_POSITION / 3, y: 0, z: POINT_POSITION / 3 },
//   { x: POINT_POSITION / 3 * 2, y: 0, z: POINT_POSITION / 3 },
//   { x: POINT_POSITION / 3, y: POINT_POSITION / 3, z: POINT_POSITION / 3 },
//   { x: POINT_POSITION / 3 * 2, y: POINT_POSITION / 3, z: POINT_POSITION / 3 },
// )

// points = sortPointsByAxes(points, ['y', 'z', 'x'], true)

points = sortPointsByAxes(points, ['y', 'z', 'x'], [false, false, true])

const vertices = []

for (let i = 0; i < points.length; i++) {
  const { x, y, z } = points[i]

  vertices.push(x, y, z)
}

const geometry = new THREE.BufferGeometry()
geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3 ))

const material = new THREE.PointsMaterial({ color: '#000', size: 1 })
const scenePoints = new THREE.Points(geometry, material)

scene.add(scenePoints)

for (let i = 0; i < points.length; i++) {
  const { x, y, z } = points[i]
  const label = new Text()

  label.text = `${i}`
  label.fontSize = 2
  label.position.x = x
  label.position.y = y
  label.position.z = z
  label.color = '#f00'

  scene.add(label)

  label.sync()
}

function animate() {
  requestAnimationFrame(animate)

  renderer.render(scene, camera)
}

animate()