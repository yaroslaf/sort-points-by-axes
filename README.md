# sort-points-by-axes

## Build project

`npm run build:dev` - build in development and watch mode

`npm run build:prod` - build minified bundle

## Try it

Build minified bundle and just open `index.html` file in browser
